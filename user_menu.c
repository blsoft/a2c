//
// Created by barrow099 on 10/1/18.
//

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <bits/fcntl-linux.h>
#include "user_menu.h"

int um_present_menu(const char *question, ...) {
    while(1){
        va_list args;
        va_start(args,question);
        printf("\n%s\n",question);
        char* item = va_arg(args,char*);
        int counter = 1;
        while(strncmp(item,"",128)) {
            printf("\t%d: %s\n",counter++,item);
            item = va_arg(args, char*);
        }
        va_end(args);

        int choice = -1;
        printf("Your choice: ");
        char sCH[10];
        scanf("%s",&sCH);

        while(choice==-1) {
            char*end;
            long nu = strtol(sCH,&end,10);
            if(nu)
               choice = (int)nu;
            else {
                printf("Not a number\n");
                printf("Your choice: ");
                scanf("%s",&sCH);
            }
        }

        if(choice > 0 && choice < counter) {
            return choice;
        }else {
            printf("Invalid choice\n");
        }
    }
}

string_t um_question(const string_t question) {
    return um_question_def(question, "");
}

string_t um_question_def(const string_t question, const string_t def_value) {
    printf("\n%s",question);
    if(strncmp(def_value,"",128)) {
        printf(" [\"%s\"]",def_value);
    }
    printf(": ");

    string_t *ret;
    ret = malloc(sizeof(char) * 128);
    char line[128] = {0};

    while ((fgets(line, sizeof 127, stdin) != NULL) && (line[0] != '\n'));
    fgets(line,127,stdin);

    if(!strncmp("\n",line,1)) {
        strcpy(ret,def_value);
    }else {
        strcpy(ret,line);
    }


    return ret;
}
