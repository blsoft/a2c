//
// Created by barrow099 on 10/1/18.
//

#ifndef APACHE2CONFIG_USER_MENU_H
#define APACHE2CONFIG_USER_MENU_H
typedef char* string_t;
typedef struct string_ret {char data[128];} string_ret;

int um_present_menu(const char *question, ...);
string_t um_question(const string_t question);
string_t um_question_def(const string_t question, const string_t def_value);

#endif //APACHE2CONFIG_USER_MENU_H
