#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <zconf.h>
#include <fcntl.h>

#include "user_menu.h"
typedef struct ProgConfig {
    string_t apache_path;
} ProgConfig;

typedef char* string;

//Prototypes
ProgConfig* load_config();

void config_auto(ProgConfig *pConfig);

void config_manual(ProgConfig *pConfig);

bool check_apache(string_t path);

int main() {

    load_config();
    //use_interactive();
    return 0;
}

ProgConfig* load_config() {
    string path = "/etc/a2c/a2c.cfg";
    FILE *f = fopen(path,"r");
    bool exists = true;
    if(f == 0) {
        mkdir("/etc/a2c",777);
        f = fopen(path,"w");
        exists = false;
    }

    if(exists) {
        char key[64], value[64];
        while(fscanf(f,"%s = %s",key,value) != EOF) {
            printf("%s, %s", key, value);
        }
    }
    else {
        int choice = um_present_menu("Configuration not exists. How do you want to configure the program?",
                                     "Automatic mode. The program tries to configure itself (recommended)",
                                     "Manual mode. You have to specify the correct path by hand.");
        ProgConfig *pcfg = (ProgConfig*) malloc(sizeof(ProgConfig));
        if(choice == 1) {
            config_auto(pcfg);
        }else {
            config_manual(pcfg);
        }
    }
    return 0;
}

void config_manual(ProgConfig *pConfig) {
    //Apache path
    string_t apache_path;
    apache_path = um_question_def("Please enter the path of the Apache2 config folder","/etc/apache2/");
    if(check_apache(apache_path)) {
        printf("Setting Apache folder to \'%s\'\n",apache_path);
        pConfig->apache_path = apache_path;
    }else {
        printf("\nERROR: \"%s\" is not an apache2 config folder!",apache_path);
        config_manual(pConfig);
    }
}

bool check_apache(string_t path) {
    char file[128] = {0};
    strcat(file,path);
    strcat(file,"apache2.conf");
    FILE *f = fopen(file,"r");
    bool exists = true;
    if(f == 0) {
        exists = false;
    }

    return exists;
}

void config_auto(ProgConfig *pConfig) {
    printf("Checkin apache2 config in /etc/apache2 ...\n");
    if(check_apache("/etc/apache2/")) {
        printf("Found apache config in \'/etc/apache2\'\n");
        pConfig->apache_path = "/etc/apache2/";
    }else {
        string_t ret = um_question_def("Couldn't find apache2 config in the default place. Do you want to enter a custom path for it? (Y/N)","Y");
        if(!strcmp(ret,"Y")) {
            config_manual(pConfig);
        } else {
            exit(-1);
        }
    }
}